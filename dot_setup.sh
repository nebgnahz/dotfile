#!/bin/bash

# default should only dry-run
ln -sf "$PWD"/dot_aliases $HOME/.bash_aliases
ln -sf "$PWD"/dot_bash_enhancement $HOME/.bash_enhancement
ln -sf "$PWD"/dot_emacs $HOME/.emacs
ln -sf "$PWD"/dot_gitconfig $HOME/.gitconfig
ln -sf "$PWD"/dot_mrconfig $HOME/.mrconfig
ln -sf "$PWD"/dot_hledger.journal $HOME/.hledger.journal
ln -sf "$PWD"/dot_clang-format $HOME/.clang-format
