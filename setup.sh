#!/bin/sh

## This shell script helps setup a developing environment that is good
## for me :)

##  1. Install dependencies
update() {
    echo "Performing Ubuntu updates"
    sudo apt-get update
    sudo apt-get install -y  \
	 emacs  \
	 git  \
	 meld  \
	 openssh-server \
	 tmux
}

##  2. Create new keys and change permission
ssh_key() {
    ssh-keygen -t rsa -b 4096 -C "benzh@cs.berkeley.edu"
    chmod 400 ~/.ssh/id_rsa
}

##  3. Upload ssh keys to a remote server
add_key() {
    cat ~/.ssh/id_rsa.pub | ssh $1 "cat - >> \$HOME/.ssh/authorized_keys"
}

##  4. Add dot files
dot_file() {
    ln -sf "$PWD"/dot_aliases ~/.bash_aliases
    ln -sf "$PWD"/dot_bash_enhancement ~/.bash_enhancement
    ln -sf "$PWD"/dot_emacs ~/.emacs
    ln -sf "$PWD"/dot_gitconfig ~/.gitconfig
    ln -sf "$PWD"/dot_mrconfig ~/.mrconfig
}
